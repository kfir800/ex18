
#include <windows.h>
#include "Helper.h"

using namespace std;

enum commands
{
	pwd,
	cd,
	ls,
	clear,
	create
};

//return your Path
string ExePath();
//translate from string to enum 
commands translate(string s);
//cd command
void cdCom(string path);
//ls command
void lsCom();

int main()
{
	string input = "";
	std::vector<std::string> words;
	while (input != "exit")
	{
		cout << "Command-$ " << flush;
		cin >> input;
		words = Helper::get_words(input);
		switch (translate(words[0]))
		{
		case pwd:
			cout << "Your Path: " + ExePath() << endl;
			break;
		case cd:
			cdCom(words[1]);
			break;
		case ls:
			lsCom();
			break;
		case clear:
			system("cls");
			break;
		case create:
			system((string("echo.>") + words[1]).c_str());
			break;
		}
	}
	system("PAUSE");
	return 0;
}

string ExePath()
{

	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

commands translate(string s)
{
	if (s == "pwd")	return pwd;
	if (s == "cd")	return cd;
	if (s == "ls")	return ls;
	if (s == "clear")	return clear;
	if (s == "create")	return create;
}

void cdCom(string path)
{
	system((string("cd:") + path).c_str());
}

void lsCom()
{
	system("dir");
}